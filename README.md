# Cell 1.0 PoC

A proof on concept for [cell 1.0 proposal](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139519)

- Router/Entrypoint: <https://gitlab.steveazz.xyz>
  - Cell 1: <https://cell-1.steveazz.xyz>
  - Cell 2: <https://cell-2.steveazz.xyz>
- Cell-1 Login: <https://gitlab.steveazz.xyz>
  - Username: `root`
  - Password: 1password > Engineering > Cell 1.0 Poc > cell-1 (password)
  - SSH: `gcloud --project eng-core-tenant-poc-bbc34148 compute ssh cell-1-omnibus`
- Cell-2 Login: <https://gitlab.steveazz.xyz>
  - Open the browser javascript console and run `document.cookie = 'cell_2=true;path=/;domain=gitlab.steveazz.xyz'` to force cell-2 logging
  - Username: `cell_2_root`
  - Password: 1password > Engineering > Cell 1.0 Poc > cell-2 (password)
  - SSH: `gcloud --project eng-core-tenant-poc-bbc34148 compute ssh cell-2-omnibus`

## Infra

Architecture:

```plantuml

cloud "Cloudflare" {
  node "Router"
}


rectangle "Google Cloud Platform" {
  node "cell-1"
  node "cell-2"
}

"Router" -> "cell-1"
"Router" -> "cell-2"
```

1. Create machines

   ```sh
   gcloud --project eng-core-tenant-poc-bbc34148 compute instances create cell-1-omnibus --image-family ubuntu-2204-lts --image-project ubuntu-os-cloud --zone=us-east1-c --machine-type=n1-standard-4 --size 200
   gcloud --project eng-core-tenant-poc-bbc34148 compute instances create cell-2-omnibus --image-family ubuntu-2204-lts --image-project ubuntu-os-cloud --zone=us-east1-c --machine-type=n1-standard-4 --size 200
   ```

1. Install GitLab on Ubuntu <https://about.gitlab.com/install/#ubuntu>
1. Configure SSL termination to be on Cloudflare level <https://docs.gitlab.com/omnibus/settings/ssl/index.html#configure-a-reverse-proxy-or-load-balancer-ssl-termination>

## Set up PoC branch

1. Apply patch from <https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141495>

   ```sh
   curl -s --output /tmp/141495.patch https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141495.patch
   cd /opt/gitlab/embedded/service/gitlab-rails
   sudo patch -p1 -b -f < /tmp/141495.patch
   sudo gitlab-ctl restart
   ```

   [source](https://handbook.gitlab.com/handbook/support/workflows/patching_an_instance/)

1. Set [environment variables](https://docs.gitlab.com/omnibus/settings/environment-variables.html) for each Cell

   1. Update `/etc/gitlab/gitlab.rb` with the following

      ```sh
      gitlab_rails['env'] = {
        'GITLAB_CELLS_PREFIX' => "cell_2_",
        'GITLAB_CELL_URL' => 'https://cell-2.steveazz.xyz',
        'GITLAB_CELL_NAME' => 'cell_2',
        'GITLAB_PRIMARY_CELL' => 'https://cell-1.steveazz.xyz/',
        'GITLAB_SIMULATE_SAAS' => 1,
        'SUBSCRIPTION_PORTAL_URL' => 'https://customersdot-cells.us.to',
      }
      ```

   1. Reconfigure `sudo gitlab-ctl reconfigure`

## Apply individual commit patches

Because we are pushing new commits to the POC development branch, it's necessary sometimes
to apply individual commits to the OmniBus installation.

This `bash` function has been added to `~/.bashrc`:

```
function apply_commit {
  cd /opt/gitlab/embedded/service/gitlab-rails
  curl -s --output /tmp/$1.patch https://gitlab.com/gitlab-org/gitlab/-/commit/$1.patch
  sudo patch -p1 -b -f < /tmp/$1.patch
}
```

To apply any commit, just run `apply_commit <COMMIT_SHA>`. This command doesn't restart `puma`.
To restart `puma`, just run `sudo gitlab-ctl restart puma`.

## Post Install

1. Add ultimate license
1. Add licenses for users to test different licenses, follow this [fixture](https://gitlab.com/gitlab-org/gitlab/-/blob/cb4f1cee0f7ace4d9b738e566db198f75def33e5/ee/db/fixtures/development/27_plans.rb#L3-9)

   ```sh
   $ sudo gitlab-rails c
   irb(main):001:0 Plan::PAID_HOSTED_PLANS.each do |plan|
       Plan.create!(name: plan, title: plan.titleize)

       print '.'
     end
   ```
